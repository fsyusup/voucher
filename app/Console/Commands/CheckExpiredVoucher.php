<?php

namespace App\Console\Commands;

use App\Models\Voucher;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CheckExpiredVoucher extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voucher:check-expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Expired Voucher';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Voucher::whereNotNull('customer_id')->whereNull('booking_time_confirm')->where('booking_time_expired', '<', date('Y-m-d H:i:s'))
            ->update([
                "booking_time" => null,
                "booking_time_expired" => null,
                "customer_id" => null
            ]);
    }
}
