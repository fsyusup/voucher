<?php

if (!function_exists('responseFailed')) {
    function responseFailed($data = [], $message = 'FAILED', $code = 400)
    {
        return response()->json(
            [
                'status' => false,
                'data' => $data,
                'message' => $message
            ],
            $code
        );
    }
}

if (!function_exists('responseSuccess')) {
    function responseSuccess($data = [], $message = 'OK', $code = 200)
    {
        return response()->json(
            [
                'status' => true,
                'data' => $data,
                'message' => $message
            ],
            $code
        );
    }
}
