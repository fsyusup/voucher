<?php

namespace App\Http\Controllers\API;

use App\Models\Voucher;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\PurchaseTransaction;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CampaignController extends Controller
{
    public function check(Request $request)
    {
        $data = $request->all();
        try {
            $messages = [
                'customer_id.unique' => 'You have booked a voucher before!',
            ];

            $validator = Validator::make($data, [
                'customer_id' => 'required|exists:customers,id|unique:vouchers,customer_id'
            ], $messages);

            if ($validator->fails()) {
                return responseFailed([], $validator->errors()->first());
            }

            $min_trx = 3;
            $min_spent = 300;
            $purchase = PurchaseTransaction::where('customer_id', $data['customer_id'])->where('transaction_at', Carbon::now()->subDays(30));

            if ($purchase->count() >= $min_trx && $purchase->sum('total_spent') >= $min_spent) {
                $model = Voucher::whereNull('booking_time')->whereNull('booking_time_confirm')->whereNull('customer_id')->lockForUpdate()->first();
                if ($model) {
                    $bookingTime = date('Y-m-d H:i:s');
                    $bookingTimeExpired = date('Y-m-d H:i:s', strtotime("+10 minutes", strtotime($bookingTime)));
                    $model->booking_time = $bookingTime;
                    $model->booking_time_expired = $bookingTimeExpired;
                    $model->customer_id = $data['customer_id'];
                    $model->save();

                    return responseSuccess(['code' => $model->code], 'Congratulations, you have booked a voucher!');
                }
                return responseFailed([], 'No voucher available');
            }

            return responseFailed([], "Sorry, you're not eligible!");
        } catch (\Exception $e) {
            return responseFailed($data, $e->getMessage());
        }
    }

    public function redeem(Request $request)
    {
        $data = $request->all();
        try {
            $messages = [
                'customer_id.unique'    => 'You have redeem a voucher before!',
                'customer_id.exists'    => 'No voucher booked for you!',
            ];

            $validator = Validator::make($data, [
                'customer_id' => 'required|exists:vouchers,customer_id'
            ], $messages);

            if ($validator->fails()) {
                return responseFailed([], $validator->errors()->first());
            }

            // fake image recognition
            $faker = Faker::create();
            $imageRecognition = $faker->randomElement([true, false]);
            $model = Voucher::where('customer_id', $data['customer_id'])->first();

            if (strtotime($model->booking_time_expired) > strtotime(date('Y-m-d H:i:s'))) {
                if (!empty($model->booking_time_confirm)) {
                    return responseFailed(['code' => $model->code], 'Your have redeem a voucher before!');
                } else {
                    if ($imageRecognition) {
                        $model->booking_time_confirm = date('Y-m-d H:i:s');
                        $model->save();
                        return responseFailed(['code' => $model->code], 'Congratulations, your have redeem a voucher!');
                    } else {
                        return responseFailed([], 'Your photo is not valid!');
                    }
                }
            } else {
                $model->customer_id = null;
                $model->booking_time = null;
                $model->booking_time_expired = null;

                return responseFailed([], 'Sorry, your booking voucher is expired!');
            }
        } catch (\Exception $e) {
            return responseFailed($data, $e->getMessage());
        }
    }
}
