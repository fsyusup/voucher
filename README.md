## Installation

Clone Project `https://gitlab.com/fsyusup/voucher`
Copy `.env.example` to `.env`.

Run commands :

```shell
# Install Project
composer install

# Migration Database
php artisan migrate

# Seed Database
php artisan db:seed --class=CustomerSeeder
php artisan db:seed --class=PurchaseTransactionSeeder
php artisan db:seed --class=VoucherSeeder

#Register Cron Job
php artisan schedule:run
```

## Rest API

Voucher Validation Check
```ruby
POST "/api/campaign/check"
{
    "customer_id" : $id
}
```

Redem Voucher
```ruby
POST "/api/campaign/redeem"
{
    "customer_id" : $id
}
```