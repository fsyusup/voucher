<?php

namespace Database\Seeders;

use App\Models\Customer;
use App\Models\PurchaseTransaction;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PurchaseTransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = Customer::select('id')->get();
        $arrCustomerId = [];
        foreach ($customers as $key => $value) {
            $arrCustomerId[] = $value['id'];
        }

        $faker = Faker::create();
        for ($i = 0; $i < 500; $i++) {
            PurchaseTransaction::insert([
                'customer_id' => $faker->randomElement($arrCustomerId),
                'total_spent' => $faker->numberBetween(20, 100),
                'total_saving' => $faker->numberBetween(0, 10),
                'transaction_at' => $faker->dateTimeBetween($startDate = '-2 month', $endDate = 'now', $timezone = null)
            ]);
        }
    }
}
