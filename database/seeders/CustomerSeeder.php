<?php

namespace Database\Seeders;

use App\Models\Customer;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();
        for ($i = 0; $i < 20; $i++) {

            $gender = $faker->randomElement(['male', 'female']);
            Customer::insert([
                'first_name' => $faker->firstName($gender),
                'last_name' => $faker->lastName($gender),
                'gender' => $gender,
                'date_of_birth' => $faker->dateTimeBetween($startDate = '-30 years', $endDate = 'now', $timezone = null),
                'contact_number' => $faker->phoneNumber(),
                'email' => $faker->email()
            ]);
            # code...
        }
        //
    }
}
