<?php

namespace Database\Seeders;

use App\Models\Voucher;
use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class VoucherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();
        for ($i = 0; $i < 1000; $i++) {
            Voucher::insert([
                'code' => $faker->regexify('[A-Za-z0-9]{10}')
            ]);
        }
    }
}
